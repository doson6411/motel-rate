﻿namespace MotelRate.Models
{
    public class RateMotel
    {
        public string accountFullName { get; set; }

        public string commentMotel { get; set; }

        public int  idMotel { get; set; }

        public double? avgMotel { get; set; }
    }
}
