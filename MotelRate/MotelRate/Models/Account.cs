﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MotelRate.Models
{
    public partial class Account
    {
        public Account()
        {
            Motels = new HashSet<Motel>();
            Rates = new HashSet<Rate>();
        }

        public int AccountId { get; set; }
        public string UserName { get; set; } = null!;
        [DataType(DataType.Password)]
        public string? Password { get; set; } = null!;
        public string FullName { get; set; } = null!;
        public string Phone { get; set; } = null!;
        public DateTime Dob { get; set; }
        public string Address { get; set; } = null!;
        public int Type { get; set; }
        public bool IsActive { get; set; }

        public virtual ICollection<Motel> Motels { get; set; }
        public virtual ICollection<Rate> Rates { get; set; }

        public static implicit operator Account(Task<Account?> v)
        {
            throw new NotImplementedException();
        }
    }
}
