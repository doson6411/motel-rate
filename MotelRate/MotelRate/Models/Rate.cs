﻿using System;
using System.Collections.Generic;

namespace MotelRate.Models
{
    public partial class Rate
    {
        public int MotelId { get; set; }
        public int AccountId { get; set; }
        public string? Comment { get; set; }
        public double? NumberRating { get; set; }
        public DateTime? TimeRate { get; set; }
        public bool? Care { get; set; }

        public virtual Account Account { get; set; } = null!;
        public virtual Motel Motel { get; set; } = null!;
    }
}
