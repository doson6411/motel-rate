﻿using System;
using System.Collections.Generic;

namespace MotelRate.Models
{
    public partial class Motel
    {
        public Motel()
        {
            Rates = new HashSet<Rate>();
        }

        public int MotelId { get; set; }
        public string MotelName { get; set; } = null!;
        public string MotelAddress { get; set; } = null!;
        public int AllQuantity { get; set; }
        public int EmptyQuantity { get; set; }
        public string MotelDescription { get; set; } = null!;
        public string MotelImage { get; set; } = null!;
        public int? AccountId { get; set; }
        public bool MotelIsActive { get; set; }
        public double RateAveraged { get; set; }
        public decimal? Price { get; set; }

        public virtual Account? Account { get; set; }
        public virtual ICollection<Rate> Rates { get; set; }
    }
}
