﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace MotelRate.Models
{
    public partial class MotelDBContext : DbContext
    {
        public MotelDBContext()
        {
        }

        public MotelDBContext(DbContextOptions<MotelDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Account> Accounts { get; set; } = null!;
        public virtual DbSet<Motel> Motels { get; set; } = null!;
        public virtual DbSet<Rate> Rates { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=DOTHANHSON\\DOSON;Database=MotelDB;uid=sa;pwd=123456");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>(entity =>
            {
                entity.ToTable("Account");

                entity.Property(e => e.AccountId).HasColumnName("AccountID");

                entity.Property(e => e.Address).HasMaxLength(100);

                entity.Property(e => e.Dob).HasColumnType("date");

                entity.Property(e => e.FullName).HasMaxLength(100);

                entity.Property(e => e.Password).HasMaxLength(100);

                entity.Property(e => e.Phone).HasMaxLength(10);

                entity.Property(e => e.UserName).HasMaxLength(100);
            });

            modelBuilder.Entity<Motel>(entity =>
            {
                entity.ToTable("Motel");

                entity.Property(e => e.MotelId).HasColumnName("MotelID");

                entity.Property(e => e.AccountId).HasColumnName("AccountID");

                entity.Property(e => e.MotelAddress).HasMaxLength(100);

                entity.Property(e => e.MotelDescription).HasMaxLength(1000);

                entity.Property(e => e.MotelImage).HasMaxLength(1000);

                entity.Property(e => e.MotelName).HasMaxLength(100);

                entity.Property(e => e.Price)
                    .HasColumnType("money")
                    .HasColumnName("price");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Motels)
                    .HasForeignKey(d => d.AccountId)
                    .HasConstraintName("FK__Motel__AccountID__3C69FB99");
            });

            modelBuilder.Entity<Rate>(entity =>
            {
                entity.HasKey(e => new { e.MotelId, e.AccountId })
                    .HasName("PK_Person");

                entity.Property(e => e.MotelId).HasColumnName("MotelID");

                entity.Property(e => e.AccountId).HasColumnName("AccountID");

                entity.Property(e => e.Comment).HasMaxLength(1000);

                entity.Property(e => e.TimeRate).HasColumnType("datetime");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Rates)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Rates__AccountID__3F466844");

                entity.HasOne(d => d.Motel)
                    .WithMany(p => p.Rates)
                    .HasForeignKey(d => d.MotelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Rates__MotelID__3E52440B");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
