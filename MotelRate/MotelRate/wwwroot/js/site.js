﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/hub").build();

connection.on("ReloadRateMotel", (data) => {
    debugger;
    var s = "";
    var idMotel = $("#MotelID").val();
    if (data[0].idMotel == idMotel) {
        $.each(data, (k, v) => {
            s += `<h6>${v.accountFullName}: ${v.commentMotel}</h6><br/>`
        });
        $("#rateMotel").html(s);
        $("#avg").html("Rate: " + data[0].avgMotel + "/ 10.0");
    } else {
        return;
    }
});

connection.start().then().catch(function (err) {
    return console.log(err.toString());
});
