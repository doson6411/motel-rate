﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MotelRate.Models;

namespace MotelRate.Pages.Admin.Motels
{
    public class EditModel : PageModel
    {
        private readonly MotelRate.Models.MotelDBContext _context;

        public EditModel(MotelRate.Models.MotelDBContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Motel Motel { get; set; } = default!;

        public List<Rate>? rates { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            string? accountString = HttpContext.Session.GetString("account");
            Account? acc = JsonSerializer.Deserialize<Account>(accountString);
            if (acc == null)
            {
                return RedirectToPage("/Accounts/Login");
            }
            else if (acc.Type != 0)
            {
                if (acc.Type == 2)
                {
                    return RedirectToPage("/Index");
                }
                else
                {
                    return RedirectToPage("/Host/Index");
                }
            }
            if (id == null || _context.Motels == null)
            {
                return NotFound();
            }

            var motel = await _context.Motels.Include(m => m.Account).FirstOrDefaultAsync(m => m.MotelId == id);
            rates = await _context.Rates.Include(r => r.Account).Where(r => r.MotelId == id).ToListAsync();
            if (motel == null)
            {
                return NotFound();
            }
            Motel = motel;
            ViewData["AccountId"] = new SelectList(_context.Accounts, "AccountId", "Address");
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            Motel motel = _context.Motels.Where(m => m.MotelId == Motel.MotelId).FirstOrDefault();
            motel.MotelIsActive = motel.MotelIsActive == true ? false : true;
            _context.Attach(motel).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MotelExists(Motel.MotelId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool MotelExists(int id)
        {
            return (_context.Motels?.Any(e => e.MotelId == id)).GetValueOrDefault();
        }
    }
}
