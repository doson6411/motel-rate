﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MotelRate.Models;

namespace MotelRate.Pages.Admin.Members
{
    public class EditModel : PageModel
    {
        private readonly MotelRate.Models.MotelDBContext _context;

        public EditModel(MotelRate.Models.MotelDBContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Account Account { get; set; } = default!;

        public List<SelectListItem> Types { set; get; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            string? accountString = HttpContext.Session.GetString("account");
            Account? acc = JsonSerializer.Deserialize<Account>(accountString);
            if (acc == null)
            {
                return RedirectToPage("/Accounts/Login");
            }
            else if (acc.Type != 0)
            {
                if (acc.Type == 2)
                {
                    return RedirectToPage("/Index");
                }
                else
                {
                    return RedirectToPage("/Host/Index");
                }
            }
            Types = new List<SelectListItem> {
                new SelectListItem{Text = "Admin", Value = "0"},
                new SelectListItem{Text = "Host", Value = "1"},
                new SelectListItem{Text = "Customer", Value = "2"},
            };
            if (id == null || _context.Accounts == null)
            {
                return NotFound();
            }

            var account =  await _context.Accounts.FirstOrDefaultAsync(m => m.AccountId == id);
            if (account == null)
            {
                return NotFound();
            }
            Account = account;
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Account).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AccountExists(Account.AccountId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool AccountExists(int id)
        {
          return (_context.Accounts?.Any(e => e.AccountId == id)).GetValueOrDefault();
        }
    }
}
