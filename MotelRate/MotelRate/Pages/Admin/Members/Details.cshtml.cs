﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MotelRate.Models;

namespace MotelRate.Pages.Admin.Members
{
    public class DetailsModel : PageModel
    {
        private readonly MotelRate.Models.MotelDBContext _context;

        public DetailsModel(MotelRate.Models.MotelDBContext context)
        {
            _context = context;
        }

      public Account Account { get; set; } = default!; 

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            string? accountString = HttpContext.Session.GetString("account");
            Account? acc = JsonSerializer.Deserialize<Account>(accountString);
            if(acc == null)
            {
                return RedirectToPage("/Accounts/Login");
            } else if (acc.Type != 0)
            {
                if(acc.Type == 2)
                {
                    return RedirectToPage("/Index");
                }
                else
                {
                    return RedirectToPage("/Host/Index");
                }
            }
            if (id == null || _context.Accounts == null)
            {
                return NotFound();
            }

            var account = await _context.Accounts.FirstOrDefaultAsync(m => m.AccountId == id);
            if (account == null)
            {
                return NotFound();
            }
            else 
            {
                Account = account;
            }
            return Page();
        }
    }
}
