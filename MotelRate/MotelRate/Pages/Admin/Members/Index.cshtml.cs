﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MotelRate.Models;

namespace MotelRate.Pages.Admin.Members
{
    public class IndexModel : PageModel
    {
        private readonly MotelRate.Models.MotelDBContext _context;

        public IndexModel(MotelRate.Models.MotelDBContext context)
        {
            _context = context;
        }

        public IList<Account> Account { get;set; } = default!;

        public async Task<IActionResult> OnGetAsync()
        {
            string? account = HttpContext.Session.GetString("account");
            Account acc = JsonSerializer.Deserialize<Account>(account);
            if (acc == null)
            {
                return RedirectToPage("/Accounts/Login");
            }
            else if (acc.Type != 0)
            {
                if (acc.Type == 2)
                {
                    return RedirectToPage("/Index");
                }
                else
                {
                    return RedirectToPage("/Host/Index");
                }
            }
            if (_context.Accounts != null)
            {
                Account = await _context.Accounts.Where(a => a.AccountId != acc.AccountId).ToListAsync();
            }
            return Page();
        }
    }
}
