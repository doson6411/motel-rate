using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using MotelRate.Models;
using System.Text.Json;

namespace MotelRate.Pages.Accounts
{
    public class LoginModel : PageModel
    {
        private readonly MotelDBContext _db;

        public LoginModel(MotelDBContext db)
        {
            _db = db;
        }
        [BindProperty]
        public Account Account { get; set; }

        public string ErrMsg { get; set; }
        public IActionResult OnGet()
        {
            return Page();
        }

        public IActionResult OnPost()
        {
            ErrMsg = string.Empty;
            if (ModelState.ErrorCount > 3)
            {
                return Page();
            }
            Account? acc = _db.Accounts.Where(acc => acc.UserName.Equals(Account.UserName)
                        && acc.Password.Equals(Account.Password)).FirstOrDefault();
            if (acc == null)
            {
                ErrMsg = "Account do not exist!";
                return Page();
            }
            string accountString = JsonSerializer.Serialize(acc);
            HttpContext.Session.SetString("account", accountString);
            if (acc.Type == 0)
            {
                return RedirectToPage("/Admin/Motels/Index");
            }
            else if (acc.Type == 1)
            {
                return RedirectToPage("/Host/Index");
            }
            else
            {
                return RedirectToPage("/Index");
            }
        }
    }
}
