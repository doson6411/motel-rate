﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using MotelRate.Models;

namespace MotelRate.Pages.Accounts
{
    public class CreateModel : PageModel
    {
        private readonly MotelRate.Models.MotelDBContext _context;

        public CreateModel(MotelRate.Models.MotelDBContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Account Account { get; set; } = default!;

        public string ErrMsg { get; set; }


        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            ErrMsg = string.Empty;
            if (!ModelState.IsValid || _context.Accounts == null || Account == null)
            {
                return Page();
            }
            Account.Type = 2;
            Account.IsActive = true;
            Account? a = _context.Accounts.FirstOrDefault(x => x.UserName.ToUpper().Equals(Account.UserName.ToUpper()));
            if (a != null)
            {
                ErrMsg = "Username Exists!";
                return Page();
            }
            _context.Accounts.Add(Account);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Login");
        }
    }
}
