﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using MotelRate.Models;

namespace MotelRate.Pages.Host
{
    public class CreateModel : PageModel
    {
        private readonly MotelRate.Models.MotelDBContext _context;

        public CreateModel(MotelRate.Models.MotelDBContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            string? accountString = HttpContext.Session.GetString("account");
            Account? acc = JsonSerializer.Deserialize<Account>(accountString);
            if (acc == null)
            {
                return RedirectToPage("/Accounts/Login");
            }
            else if (acc.Type != 1)
            {
                if (acc.Type == 2)
                {
                    return RedirectToPage("/Index");
                }
                else
                {
                    return RedirectToPage("/Admin/Motels/Index");
                }
            }
            return Page();
        }

        [BindProperty]
        public Motel Motel { get; set; } = default!;

        public string ErrMsg { get; set; }
        

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            ErrMsg = string.Empty;
          if (!ModelState.IsValid || _context.Motels == null || Motel == null)
            {
                return Page();
            }
          if(this.Motel.AllQuantity < this.Motel.EmptyQuantity)
            {
                ErrMsg = "Create Faild";
                return Page();
            }
            string? account = HttpContext.Session.GetString("account");
            Account acc = JsonSerializer.Deserialize<Account>(account);
            this.Motel.AccountId = acc.AccountId;
            this.Motel.RateAveraged = 0;
            this.Motel.MotelIsActive= false;
            _context.Motels.Add(Motel);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
