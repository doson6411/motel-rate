﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MotelRate.Models;

namespace MotelRate.Pages.Host
{
    public class DeleteModel : PageModel
    {
        private readonly MotelRate.Models.MotelDBContext _context;

        public DeleteModel(MotelRate.Models.MotelDBContext context)
        {
            _context = context;
        }

        [BindProperty]
      public Motel Motel { get; set; } = default!;

        public string ErrMsg { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            string? accountString = HttpContext.Session.GetString("account");
            Account? acc = JsonSerializer.Deserialize<Account>(accountString);
            if (acc == null)
            {
                return RedirectToPage("/Accounts/Login");
            }
            else if (acc.Type != 1)
            {
                if (acc.Type == 2)
                {
                    return RedirectToPage("/Index");
                }
                else
                {
                    return RedirectToPage("/Admin/Motels/Index");
                }
            }
            if (id == null || _context.Motels == null)
            {
                return NotFound();
            }

            var motel = await _context.Motels.Include(m => m.Account).FirstOrDefaultAsync(m => m.MotelId == id);

            if (motel == null)
            {
                return NotFound();
            }
            else 
            {
                Motel = motel;
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null || _context.Motels == null)
            {
                return NotFound();
            }
            var motel = await _context.Motels.FindAsync(id);
            var rateOfMotel = _context.Rates.Where(r => r.MotelId == motel.MotelId).ToList();
            if(rateOfMotel.Count() > 0)
            {
                this.Motel = await _context.Motels.Include(m => m.Account).FirstOrDefaultAsync(m => m.MotelId == id);
                ErrMsg = "Cannot delete motel. Because the motel has been rated";
                return Page();
            }

            if (motel != null)
            {
                Motel = motel;
                _context.Motels.Remove(Motel);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
