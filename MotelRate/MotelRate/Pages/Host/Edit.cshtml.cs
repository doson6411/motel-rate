﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MotelRate.Models;

namespace MotelRate.Pages.Host
{
    public class EditModel : PageModel
    {
        private readonly MotelRate.Models.MotelDBContext _context;

        public EditModel(MotelRate.Models.MotelDBContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Motel Motel { get; set; } = default!;

        public string ErrMsg { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            string? accountString = HttpContext.Session.GetString("account");
            Account? acc = JsonSerializer.Deserialize<Account>(accountString);
            if (acc == null)
            {
                return RedirectToPage("/Accounts/Login");
            }
            else if (acc.Type != 1)
            {
                if (acc.Type == 2)
                {
                    return RedirectToPage("/Index");
                }
                else
                {
                    return RedirectToPage("/Admin/Motels/Index");
                }
            }
            if (id == null || _context.Motels == null)
            {
                return NotFound();
            }

            var motel =  await _context.Motels.Include(m => m.Account).FirstOrDefaultAsync(m => m.MotelId == id);
            if (motel == null)
            {
                return NotFound();
            }
            Motel = motel;
           ViewData["AccountId"] = new SelectList(_context.Accounts, "AccountId", "AccountId");
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            ErrMsg = string.Empty;
            if (!ModelState.IsValid)
            {
                return Page();
            }
            if (this.Motel.AllQuantity < this.Motel.EmptyQuantity)
            {
                ErrMsg = "Edit Faild";
                return Page();
            }

            _context.Attach(Motel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MotelExists(Motel.MotelId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool MotelExists(int id)
        {
          return (_context.Motels?.Any(e => e.MotelId == id)).GetValueOrDefault();
        }
    }
}
