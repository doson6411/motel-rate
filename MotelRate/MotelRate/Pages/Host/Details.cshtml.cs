﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MotelRate.Models;

namespace MotelRate.Pages.Host
{
    public class DetailsModel : PageModel
    {
        private readonly MotelRate.Models.MotelDBContext _context;

        public DetailsModel(MotelRate.Models.MotelDBContext context)
        {
            _context = context;
        }

      public Motel Motel { get; set; } = default!;

        public List<Rate>? rates { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            string? accountString = HttpContext.Session.GetString("account");
            Account? acc = JsonSerializer.Deserialize<Account>(accountString);
            if (acc == null)
            {
                return RedirectToPage("/Accounts/Login");
            }
            else if (acc.Type != 1)
            {
                if (acc.Type == 2)
                {
                    return RedirectToPage("/Index");
                }
                else
                {
                    return RedirectToPage("/Admin/Motels/Index");
                }
            }
            if (id == null || _context.Motels == null)
            {
                return NotFound();
            }

            var motel = await _context.Motels.Include(m => m.Account).FirstOrDefaultAsync(m => m.MotelId == id);
            rates = await _context.Rates.Include(r => r.Account).Where(r => r.MotelId == id).ToListAsync();
            if (motel == null)
            {
                return NotFound();
            }
            else 
            {
                Motel = motel;
            }
            return Page();
        }
    }
}
