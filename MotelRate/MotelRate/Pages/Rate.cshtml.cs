using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using MotelRate.Hubs;
using MotelRate.Models;
using System.Text.Json;

namespace MotelRate.Pages
{
    public class RateModel : PageModel
    {
        private readonly MotelDBContext _db;
        private readonly IHubContext<RateHub> _hubContext;

        public RateModel(MotelDBContext db, IHubContext<RateHub> hubContext)
        {
            _db = db;
            _hubContext = hubContext;
        }
        [BindProperty]
        public Motel Motel { get; set; }

        [BindProperty]
        public Rate? rate { get; set; }

        public List<Rate>? rates { get; set; }

        public string ErrMsg { get; set; }
        public IActionResult OnGet( int motelId)
        {
            string? account = HttpContext.Session.GetString("account");
            if(account == null)
            {
                return RedirectToPage("/Accounts/Login");
            }
            Account acc = JsonSerializer.Deserialize<Account>(account);
            rate = _db.Rates.Where(r => r.AccountId == acc.AccountId && r.MotelId == motelId).FirstOrDefault();
            if(rate == null)
            {
                rate = new Rate();
                rate.MotelId = motelId;
                rate.AccountId = acc.AccountId;
            }
            rates = _db.Rates.Include(r => r.Account).Where(r => r.MotelId == motelId).ToList();
            Motel = _db.Motels.Include(m => m.Account).Where(m => m.MotelId == motelId).FirstOrDefault();
            return Page();
        }

        public async Task<IActionResult> OnPost()
        {
            string? account = HttpContext.Session.GetString("account");
            Account? acc = JsonSerializer.Deserialize<Account>(account);
            int? motelId = rate.MotelId;
            Motel = await _db.Motels.Include(m => m.Account).Where(m => m.MotelId == motelId).FirstOrDefaultAsync();
            rates = await _db.Rates.Include(r => r.Account).Where(r => r.MotelId == motelId).ToListAsync();
            if (rate.NumberRating > 10 || rate.NumberRating < 0)
            {
                ErrMsg = "Point rate >= 0 && <= 10";
                return Page();
            }
            Rate? rateOld = await _db.Rates.Where(r => r.AccountId == acc.AccountId && r.MotelId == rate.MotelId).FirstOrDefaultAsync();
            if(rateOld == null)
            {
                rate.TimeRate = DateTime.Now;
                _db.Rates.Add(rate);
                await _db.SaveChangesAsync();
            }
            else
            {
                rateOld.Comment = rate.Comment;
                rateOld.TimeRate = DateTime.Now;
                rateOld.NumberRating= rate.NumberRating;
                _db.Entry(rateOld).State= EntityState.Modified;
                await _db.SaveChangesAsync();
            }
            Motel? mo = await _db.Motels.Where(m => m.MotelId == rate.MotelId).FirstOrDefaultAsync();
            List<Rate> listRateOfMotel = await _db.Rates.Where(r => r.MotelId == mo.MotelId).ToListAsync();
            if(listRateOfMotel.Count > 0)
            {
                double? sum = 0;
                foreach(Rate r in listRateOfMotel)
                {
                    sum += r.NumberRating;
                }
                mo.RateAveraged = (double)(sum /(double)listRateOfMotel.Count());
            }
            else
            {
                mo.RateAveraged = 0;
            }
            _db.Entry(mo).State = EntityState.Modified;
            await _db.SaveChangesAsync();
            await _hubContext.Clients.All.SendAsync("ReloadRateMotel", await (from r in _db.Rates
                                                                              join a in _db.Accounts on r.AccountId equals a.AccountId
                                                                              join m in _db.Motels on r.MotelId equals m.MotelId
                                                                              where r.MotelId == mo.MotelId
                                                                              select new RateMotel
                                                                              {
                                                                                  accountFullName = a.FullName,
                                                                                  commentMotel = r.Comment,
                                                                                  idMotel = r.MotelId,
                                                                                  avgMotel = m.RateAveraged
                                                                              }).ToListAsync());
            return Redirect("/Details?motelId=" + rate.MotelId);
        }
    }
}
