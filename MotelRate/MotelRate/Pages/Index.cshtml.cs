﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MotelRate.Models;

namespace MotelRate.Pages
{
    public class IndexModel : PageModel
    {
        private readonly MotelDBContext _db;

        public IndexModel(MotelDBContext db)
        {
            _db = db;
        }
        public List<Motel> motels { get; set; }

        public int countRate { get; set; }

        public void OnGet()
        {
            motels = _db.Motels.Include(m => m.Account).Include(m => m.Rates).Where(m => m.MotelIsActive == true).ToList();
        }
    }
}