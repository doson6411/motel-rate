using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MotelRate.Models;
using System.Text.Json;

namespace MotelRate.Pages
{
    public class DetailsModel : PageModel
    {
        private readonly MotelDBContext _db;

        public DetailsModel(MotelDBContext db)
        {
            _db = db;
        }
        public Motel Motel { get; set; }

        public List<Rate>? rates { get; set; }
        public async Task OnGet(int motelId)
        {
            Motel = await _db.Motels.Include(m => m.Account).Where(m => m.MotelId == motelId).FirstOrDefaultAsync();
            rates = await _db.Rates.Include(r => r.Account).Where(r => r.MotelId == motelId).ToListAsync();
        }
    }
}
