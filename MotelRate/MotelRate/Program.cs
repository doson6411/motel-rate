using Microsoft.EntityFrameworkCore;
using MotelRate.Hubs;
using MotelRate.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddRazorPages();

builder.Services.AddDbContext<MotelDBContext>(opt =>
{
    opt.UseSqlServer(builder.Configuration.GetConnectionString("MotelDB"));
});

builder.Services.AddSession(opt =>
{
    opt.IdleTimeout = TimeSpan.FromMinutes(30);
});
builder.Services.AddSignalR();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
}
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapRazorPages();

app.UseSession();

app.MapHub<RateHub>("/hub");

app.Run();
